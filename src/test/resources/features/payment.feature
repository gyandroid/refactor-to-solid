Feature: Payment
  I want to pay so I can go home

  Scenario Outline: Per hour payment
    Given I entered the parking at <entry_time>
    When I pay at <payment_time>
    Then I should pay <amount_to_pay>

    Examples: 
      | entry_time          | payment_time        | amount_to_pay |
      | 0001-01-01 10:00:00 | 0001-01-01 10:09:59 |             0 |
      | 0001-01-01 10:00:00 | 0001-01-01 10:10:00 |            10 |
      | 0001-01-01 10:00:00 | 0001-01-01 11:09:59 |            10 |
      | 0001-01-01 10:00:00 | 0001-01-01 11:10:00 |            20 |
