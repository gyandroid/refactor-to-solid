package devcraft.parking;


import devcraft.parking.jpa.ParkingEntryRepository;

public class ParkingService {

    private ParkingEntryRepository repository;
    private Clock clock;

    public interface Clock {
        long now();
    }

    public ParkingService(Clock clock, ParkingEntryRepository repository) {
        this.clock = clock;
        this.repository = repository;
    }

    public long enterParking() {
        ParkingEntryRepository.ParkingEntry entry = new ParkingEntryRepository.ParkingEntry();
        entry.setTime(clock.now());
        repository.save(entry);
        return entry.getCode();
    }

    public int calcPayment(long code) {
        ParkingEntryRepository.ParkingEntry entry = repository.findOne(code);
        long time = entry.getTime();
        return calcPayment(time,clock.now());
    }

    private int calcPayment(long entryTime, long paymentTime) {
        long timeInParking = paymentTime - entryTime;
        long hoursToPay = (timeInParking + 50 * 60 * 1000) / (60 * 60 * 1000);
        return (int) (10 * hoursToPay);
    }

}